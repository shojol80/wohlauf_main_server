<?php

/*

type: layout

name: Default

description: Default

*/
?>

<div class="<?php print $config['module_class'] ?>">
    <div id="<?php print $rand; ?>">
        <label>
            <?php _e("Land auswählen"); ?><span style="color: red;">*</span>
        </label>
        <!-- <span style="color: red;">*</span> -->

        <?php  $selected_country = $tax = DB::table('users')
            ->select('tax_rates.charge','users.country')
            ->join('tax_rates','tax_rates.country','=','users.country')
            ->where('users.id',user_id())->first();

        if(!@$selected_country){
            $selected_country  = $tax =  DB::table('tax_rates')->where('is_default',1)->first();
        }
        if(!@is_logged() and @mw()->user_manager->session_get("country")){
            $selected_country->country = @mw()->user_manager->session_get("country") ?? null;
        }
        ?>

        <select name="country" class="field-full form-control countrySelectBox country-rate" id="country_set" <?php if(is_logged()) {?> style="pointer-events: none !important;" <?php } ?> required>
            <?php foreach($data  as $item): ?>
                <option value="<?php print $item['shipping_country'] ?>"  <?php if(isset($selected_country) and $selected_country->country == $item['shipping_country']): ?> selected="selected" <?php endif; ?>><?php print $item['shipping_country'] ?></option>
            <?php endforeach ; ?>
        </select>
    </div>

    <module type="custom_fields" template="bootstrap3" id="shipping-info-address-<?php print $params['id'] ?>" data-for="module"  default-fields="address" input-class="field-full form-control" data-skip-fields="country"   />

</div>
