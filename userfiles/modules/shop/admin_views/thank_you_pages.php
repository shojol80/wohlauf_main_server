<style>


.thank-switch {
    position: relative;
    display: inline-block;
    width: 30px;
    height: 17px;
    margin-right: 10px;
    margin-bottom: 0;
}

.thank-switch input {
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 13px;
  width: 13px;
  left: 2px;
  bottom: 2px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(13px);
  -ms-transform: translateX(13px);
  transform: translateX(13px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
/* For thank-Switch checkbox End*/

/* Button */
.edit-btn {
    background-color: #4CAF50;
    border: none;
    color: white;
    padding: 5px 12px;
    text-align: center;
    border-radius: 5px;
    text-decoration: none;
    display: inline-block;
    font-size: 13px;
    margin: 4px 2px;
    transition-duration: 0.4s;
    cursor: pointer;
}

.first-product-name{
    width: 280px;
}
.first-product-name span{
    word-break: break-word;
    font-weight:600;
}

.first-product-name a{
    word-break: break-word;
    font-weight:600;
}

.first-product-name,
.first-checkbox,
.first-edit-btn,
.thnk-u {
    margin-right: 30px;
}

.first-part,
.second-part {
    border: 1px solid #ccc;
    margin-bottom: 30px;
    padding: 20px 40px;
}

.default-wrapper{
    display: flex;
    flex-direction:row;
    align-items:center;
}

.radio-btn,
.second-radio-btn {
    margin-bottom: 20px;
}

.thank-you-section {
    display: flex;
    flex-direction: row;
    align-items: flex-start;
    margin-bottom: 20px;
}

.thnk-u{
    display: flex;
    align-items: center;
}


</style>
<?php
        use Illuminate\Support\Facades\DB;

    ?>





<div class="default-check" id="default-div">
    <div class="first-part">
        <!-- <div class="radio-btn">
            <input type="radio" id="test" name="aaa" value="aaa">
            <label for="default">Default</label>
        </div> -->
        <div class="default-wrapper">
            <div>
                <div class="thnk-u">
                    <label class="thank-switch">
                        <input onclick="defaulttyp()"  type="checkbox" id="default-ty">
                        <span class="slider round"></span>
                    </label>
                    <span><?php _e("Deafult Page"); ?></span>
                </div>
            </div>

            <!-- <div class="first-product-name">
                <a href="#">Laptop</a>
            </div> -->

            <div class="first-edit-btn">
                <a class="edit-btn" href="<?php print url('/thank-you') ?>" target="_blank"><?php _e("Edit"); ?></a>
            </div>

        </div>
    </div>
</div>

<div class="productModule-check" id="thank-you-div">

    <div class="second-part">
        <!-- <div class="second-radio-btn">
            <input type="radio" id="bbb" name="bbb" value="bbb">
            <label for="upselling">Upselling Process</label><br>
        </div> -->
        <div class="row">
            <div class="col-md-7">
            <?php
                $checkCount = 0;
                    for ($x = 1; $x <= 6; $x++) {
                ?>
                    <?php if(DB::table("thank_you_pages")->where('template_name',$x)->where('is_active',1)->get()->count()):  ?>
                        <?php
                            $activePage =  DB::table('thank_you_pages')->where('template_name', $x)->where('is_active', '1')->get();
                            $activePageProduct =  DB::table('content')->where('id', $activePage[0]->product_id)->get();
                            $checkCount = $x;
                        ?>
                        <div class="thank-you-section">
                            <div class="thnk-u">
                                <label class="thank-switch">
                                    <input
                                    <?php
                                        if(DB::table('thank_you_pages')->where('is_active', '1')->get()->count() ==  $x){

                                        }else{
                                            print 'disabled';
                                        }
                                    ?>
                                    onclick="check('<?php print $x; ?>')" checked type="checkbox" id="<?php print $x; ?>" value="<?php print $x; ?>">
                                    <span class="slider round"></span>
                                </label>
                                <span>Thank You-<?php print $x; ?></span>
                            </div>
                            <div class="first-product-name">
                                <span id="productName<?php print $x; ?>"> <a href="<?php print url('/admin/view:shop/action:products#action=editpage:').$activePage[0]->product_id ?>" target="_blank"><?php print $activePageProduct[0]->title ?></a> </span>
                            </div>
                            <div class="first-edit-btn" id="tempedit<?php print $x; ?>">
                                <a class="edit-btn" href="<?php print url('/thank-you?temp=').$x ?>" target="_blank">Edit</a>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="thank-you-section">
                            <?php if(DB::table("thank_you_pages")->where('template_name',$x)->get()->count()):  ?>
                                <?php  $lastPage =  DB::table('thank_you_pages')->where('template_name', $x)->get();?>
                                <?php if( $checkCount + 1 == $x): ?>
                                    <div class="thnk-u">
                                        <label class="thank-switch">
                                            <input onclick="check('<?php print $x; ?>')"  type="checkbox" id="<?php print $x; ?>" value="<?php print $x; ?>">
                                            <span class="slider round"></span>
                                        </label>
                                        <span>Thank You-<?php print $x; ?></span>
                                    </div>
                                    <div class="first-product-name">
                                        <a  id="productName<?php print $x; ?>" href="<?php print url('/admin/view:shop/action:products#action=editpage:').$lastPage[0]->product_id ?>" target="_blank"></a>
                                    </div>
                                    <div class="first-edit-btn" id="tempedit<?php print $x; ?>" style="display:none;">
                                        <a class="edit-btn" href="<?php print url('/thank-you?temp=').$x ?>" target="_blank">Edit</a>
                                    </div>
                                <?php else: ?>
                                    <div class="thnk-u">
                                        <label class="thank-switch">
                                            <input disabled onclick="check('<?php print $x; ?>')"  type="checkbox" id="<?php print $x; ?>" value="<?php print $x; ?>">
                                            <span class="slider round"></span>
                                        </label>
                                        <span>Thank You-<?php print $x; ?></span>
                                    </div>
                                    <div class="first-product-name">
                                        <span id="productName<?php print $x; ?>"></span>
                                    </div>
                                    <div class="first-edit-btn" id="tempedit<?php print $x; ?>" style="display:none;">
                                        <a class="edit-btn" href="<?php print url('/thank-you?temp=').$x ?>" target="_blank">Edit</a>
                                    </div>
                                <?php endif; ?>
                            <?php else: ?>
                                <div class="thnk-u">
                                    <label class="thank-switch">
                                        <input disabled onclick="check('<?php print $x; ?>')"  type="checkbox" id="<?php print $x; ?>" value="<?php print $x; ?>">
                                        <span class="slider round"></span>
                                    </label>
                                    <span>Thank You-<?php print $x; ?></span>
                                </div>
                                <div class="first-product-name">
                                        <span id="productName<?php print $x; ?>"></span>
                                </div>
                                <div class="first-edit-btn" id="tempedit<?php print $x; ?>" style="display:none;">
                                    <a class="edit-btn" href="<?php print url('/thank-you?temp=').$x ?>" target="_blank">Edit</a>
                                </div>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                <?php
                    }
                ?>
            </div>
            <div class="col-md-5">
                <div class="thank-you-admin">
                    <p>
                        Du kannst eine Produktempfehlung starten, nachdem dein Kunde gekauft hat. Damit du eine solche Seite einrichten kannst, ist es nötig jeder Seite ein Produkt zuzuweisen. Siehe Foto. Du kannst in den Einstellungen jedes Produkt eine solche Zuordnung treffen. Im Anschluss kehre hierher zurück und aktiviere die Seite. Du kannst die Seite dann auch individuell anpassen.
                    </p>
                    <img src="<?php print modules_url(); ?>/shop/thank_you/thank-you-image.png" alt="">
                </div>
            </div>
        </div>

    </div>
</div>




<script>
    function check(id){
        if($('#'+id).is(':checked')){
            if(id==1){
                $('#default-div').hide();
                $('.thank-you-admin').hide();
            }
            idb = parseInt(id) + 1;
            $("#"+idb).prop("disabled", false);
            if(id>1){
                idt = parseInt(id) - 1;
                $("#"+idt).prop("disabled", true);
            }

            $.ajax({
                type: "POST",
                url: "<?=api_url('activeTemplate')?>",
                data:{ template_name : id },
                success: function(response) {
                    $("#productName"+id).html(response.message);
                    $("#tempedit"+id).show();
                    console.log(response.message);
                },
                error: function(response){
                    console.log(response.responseJSON.message);
                }
            });
        }
        else{
            if(id==1){
                $('#thank-you-div').hide();
                $('#default-div').show();
                $('.thank-you-admin').show();
                // $('#default-ty').attr("checked",true);
                $("#default-ty").prop('checked', true);
            }
            idb = parseInt(id) + 1;
            $("#"+idb).prop("disabled", true);

            if(id>1){
                idt = parseInt(id) - 1;
                $("#"+idt).prop("disabled", false);
            }


            $.ajax({
                type: "POST",
                url: "<?=api_url('deactiveTemplate')?>",
                data:{ template_name : id },
                success: function(response) {
                    $("#productName"+id).html('');
                    $("#tempedit"+id).hide();
                    console.log(response.message);
                },
                error: function(response){
                    console.log(response.responseJSON.message);
                }
            });
        }
    }

    function defaulttyp() {
            if($('#default-ty').is(':checked')){
                $('#thank-you-div').hide();
            }else{
                $('#thank-you-div').show();
            }
        }

    $(document).ready(function () {
        if($('#1').is(':checked')){
            $('#thank-you-div').show();
            $('#default-div').hide();
            $('.thank-you-admin').hide();
        }else{
            $('#default-div').show();
            $('.thank-you-admin').show();
            $('#thank-you-div').hide();
            $("#default-ty").prop('checked', true);
        }
    });

</script>