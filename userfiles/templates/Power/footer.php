<div class="edit safe-mode nodrop" field="power_footer" rel="global">
    <div class="footer-top-section section pt-100 pb-60">
        <div class="container">
            <div class="row">

                <!-- Footer Widget -->
                <div class="footer-widget col-md-4 col-sm-6 col-xs-12 mb-40">
                    <h5 class="widget-title">ABOUT THE STORE</h5>
                    <p>There are many variations of passages of Lor available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even
                        slightly believable</p>
                </div>

                <!-- Footer Widget -->
                <div class="footer-widget col-md-3 col-sm-6 col-xs-12 mb-40">
                    <h5 class="widget-title">CUSTOMER SERVICE</h5>
                    <module type="menu" name="footer_menu" id="footer-navigation" template="footer"/>
                </div>

                <!-- Footer Widget -->
                <div class="footer-widget col-md-2 col-sm-6 col-xs-12 mb-40">
                    <h5 class="widget-title">MEMBERS</h5>
                    <module type="menu" name="footer_menu_2" id="footer-navigation-2" template="footer"/>
                </div>

                <!-- Footer Widget -->
                <div class="footer-widget col-md-3 col-sm-6 col-xs-12 mb-40">
                    <module type="newsletter" id="footer-newsletter"/>
                    <module type="social_links" id="footer-socials"/>
                </div>

            </div>
        </div>
    </div>
</div>

<div class="footer-bottom-section section">
    <div class="container">
        <div class="row">

            <div class="copyright text-left col-sm-6 col-xs-12">
                <p>Shopsystem & Template by <a href="https://www.droptienda.com/" target="_blank">Droptienda®</a> </p>
            </div>

            <div class="footer-menu text-right col-sm-6 col-xs-12">
                <module type="menu" name="footer_menu_3" id="footer-navigation-3" template="footer-2"/>
            </div>
        </div>
    </div>
</div>


<!-- QUICK VIEW MODAL START-->
<div id="quickViewModal" class="modal fade">
    <div class="container">
        <div class="modal-content row">
            <div class="modal-body">
                <button class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
                <!-- QuickView Product Images -->
                <div id="quick-view-content" class="row text-center">
                    <br/><br/><br/>
                    <svg xmlns="http://www.w3.org/2000/svg" width="200px" height="200px" viewBox="0 0 100 100" preserveAspectRatio="xMidYMid" class="lds-fidget-spinner">
                        <g transform="rotate(34.1054 50 50)">
                            <g transform="translate(50 50)">
                                <g ng-attr-transform="scale({{config.r}})" transform="scale(0.8)">
                                    <g transform="translate(-50 -58)">
                                        <path ng-attr-fill="{{config.c2}}"
                                              d="M27.1,79.4c-1.1,0.6-2.4,1-3.7,1c-2.6,0-5.1-1.4-6.4-3.7c-2-3.5-0.8-8,2.7-10.1c1.1-0.6,2.4-1,3.7-1c2.6,0,5.1,1.4,6.4,3.7 C31.8,72.9,30.6,77.4,27.1,79.4z"
                                              fill="#0a0a0a"/>
                                        <path ng-attr-fill="{{config.c3}}"
                                              d="M72.9,79.4c1.1,0.6,2.4,1,3.7,1c2.6,0,5.1-1.4,6.4-3.7c2-3.5,0.8-8-2.7-10.1c-1.1-0.6-2.4-1-3.7-1c-2.6,0-5.1,1.4-6.4,3.7 C68.2,72.9,69.4,77.4,72.9,79.4z"
                                              fill="#ffffff"/>
                                        <circle ng-attr-fill="{{config.c4}}" cx="50" cy="27" r="7.4" fill="#f9ae5c"/>
                                        <path ng-attr-fill="{{config.c1}}"
                                              d="M86.5,57.5c-3.1-1.9-6.4-2.8-9.8-2.8c-0.5,0-0.9,0-1.4,0c-0.4,0-0.8,0-1.1,0c-2.1,0-4.2-0.4-6.2-1.2 c-0.8-3.6-2.8-6.9-5.4-9.3c0.4-2.5,1.3-4.8,2.7-6.9c2-2.9,3.2-6.5,3.2-10.4c0-10.2-8.2-18.4-18.4-18.4c-0.3,0-0.6,0-0.9,0 C39.7,9,32,16.8,31.6,26.2c-0.2,4.1,1,7.9,3.2,11c1.4,2.1,2.3,4.5,2.7,6.9c-2.6,2.5-4.6,5.7-5.4,9.3c-1.9,0.7-4,1.1-6.1,1.1 c-0.4,0-0.8,0-1.2,0c-0.5,0-0.9-0.1-1.4-0.1c-3.1,0-6.3,0.8-9.2,2.5c-9.1,5.2-12,17-6.3,25.9c3.5,5.4,9.5,8.4,15.6,8.4 c2.9,0,5.8-0.7,8.5-2.1c3.6-1.9,6.3-4.9,8-8.3c1.1-2.3,2.7-4.2,4.6-5.8c1.7,0.5,3.5,0.8,5.4,0.8c1.9,0,3.7-0.3,5.4-0.8 c1.9,1.6,3.5,3.5,4.6,5.7c1.5,3.2,4,6,7.4,8c2.9,1.7,6.1,2.5,9.2,2.5c6.6,0,13.1-3.6,16.4-10C97.3,73.1,94.4,62.5,86.5,57.5z M29.6,83.7c-1.9,1.1-4,1.6-6.1,1.6c-4.2,0-8.4-2.2-10.6-6.1c-3.4-5.9-1.4-13.4,4.5-16.8c1.9-1.1,4-1.6,6.1-1.6 c4.2,0,8.4,2.2,10.6,6.1C37.5,72.8,35.4,80.3,29.6,83.7z M50,39.3c-6.8,0-12.3-5.5-12.3-12.3S43.2,14.7,50,14.7 c6.8,0,12.3,5.5,12.3,12.3S56.8,39.3,50,39.3z M87.2,79.2c-2.3,3.9-6.4,6.1-10.6,6.1c-2.1,0-4.2-0.5-6.1-1.6 c-5.9-3.4-7.9-10.9-4.5-16.8c2.3-3.9,6.4-6.1,10.6-6.1c2.1,0,4.2,0.5,6.1,1.6C88.6,65.8,90.6,73.3,87.2,79.2z"
                                              fill="#28292f"/>
                                    </g>
                                </g>
                            </g>
                            <animateTransform attributeName="transform" type="rotate" calcMode="linear" values="0 50 50;360 50 50" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"/>
                        </g>
                    </svg>
                    <br/><br/><br/>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- QUICK VIEW MODAL END-->

<script src="{TEMPLATE_URL}js/main-mw.js"></script>

<!--     Plugins js-->
<script src="{TEMPLATE_URL}js/plugins.js"></script>
<!--     Main js-->
<script src="{TEMPLATE_URL}js/main.js"></script>




</body>
</html>