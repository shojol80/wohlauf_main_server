<?php

/*

type: layout

name: CLEAN CONTAINER

position: 0

*/
?>

<div class="page-section section pt-60 pb-90 nodrop edit safe-mode" field="layout-skin-13-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-xs-12 allow-drop">
                <div class="mw-row">
                    <div class="mw-col" style="width:100%">
                        <div class="mw-col-container">
                            <div class="mw-empty"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>