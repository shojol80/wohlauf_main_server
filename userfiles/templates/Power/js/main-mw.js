$(window).load(function () {



    // Simple way to enable the 'placeholder' attribute for browsers that doesn't support it
    if ('placeholder' in document.createElement('input') === false) {
        mw.$("[placeholder]").each(function () {
            var el = mw.$(this), p = el.attr("placeholder");
            el.val() == '' ? el.val(p) : '';
            el.bind('focus', function (e) {
                el.val() == p ? el.val('') : '';
            });
            el.bind('blur', function (e) {
                el.val() == '' ? el.val(p) : '';
            });
        });
    }


    /* Fixed shop cart */

    if (typeof _shop === 'boolean') {
        var header = document.getElementById('header');
        mw.$(window).bind("scroll", function () {
            var cart = mw.$(".mw-cart-small", header)[0];
            var cart_module = mw.tools.firstParentWithClass(cart, 'module');
            if ($(window).scrollTop() > mw.$(header).outerHeight()) {
                mw.$(cart_module).addClass("mw-cart-small-fixed");
            }
            else {
                mw.$(cart_module).removeClass("mw-cart-small-fixed");
            }
        });
    }
});


TempateFunctions = {
    contentHeight: function () {

        /**************************************
         Minimum height for the Main Container
         **************************************/
        if (self !== top) {
            return false;
        }

        var content = document.getElementById('content-holder'),
            footer = document.getElementById('footer'),
            header = document.getElementById('header');

        mw.$(content).css('minHeight', mw.$(window).height() - mw.$(header).outerHeight(true) - mw.$(footer).outerHeight(true));

    }
}


$(document).ready(function () {

    TempateFunctions.contentHeight();
    if (typeof(mw.msg.product_added) == "undefined") {
        mw.msg.product_added = "Your product is added to the shopping cart";
    }

    mw.$(window).bind('mw.cart.add', function (event, data) {

        if (document.getElementById('AddToCartModal') === null) {

            AddToCartModal = mw.modal({

                content: AddToCartModalContent(data.product.title),

                template: 'mw_modal_basic',

                name: "AddToCartModal",

                width: 400,

                height: 240

            });

        }

        else {

            AddToCartModal.container.innerHTML = AddToCartModalContent(data.product.title);

        }

    });

});

$(window).bind('load resize', function (e) {

    TempateFunctions.contentHeight();

});


/* Petko's things */
$(document).ready(function () {
    // mw.$(".main-navigation .dynamic-menu").addClass('closed');

    mw.$(".toggle-navigation").click(function () {
        if ($('.main-navigation .dynamic-menu').hasClass('closed')) {
            mw.$(".main-navigation .dynamic-menu").fadeIn(500);
            mw.$('.main-navigation .dynamic-menu').removeClass('closed');
        } else {
            mw.$(".main-navigation .dynamic-menu").fadeOut(500);
            mw.$('.main-navigation .dynamic-menu').addClass('closed');
        }
    });

    mw.$(".close-mobile-navigation").click(function () {
        mw.$(".main-navigation .dynamic-menu").fadeOut(500);
        mw.$('.main-navigation .dynamic-menu').addClass('closed');
    });
});

$(window).resize(function () {
    if ($(window).width() > 991) {
        mw.$('.main-navigation .dynamic-menu').removeAttr('style');
    } else {
        // mw.$('.main-navigation .dynamic-menu').removeClass('closed');
    }
});
