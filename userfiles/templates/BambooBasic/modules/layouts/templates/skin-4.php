<?php

/*

type: layout

name: Products

position: 4

*/

?>

<?php
if (!$classes['padding_top']) {
    $classes['padding_top'] = 'p-t-70';
}
if (!$classes['padding_bottom']) {
    $classes['padding_bottom'] = 'p-b-70';
}

$layout_classes = ' ' . $classes['padding_top'] . ' ' . $classes['padding_bottom'] . ' ';
?>

<section class="section <?php print $layout_classes; ?> safe-mode nodrop" field="layout-skin-4-<?php print $params['id'] ?>" rel="module">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <module type="shop/products" hide_paging="true" limit="6" />
            </div>
            <div class="col-lg-3">
                <?php include TEMPLATE_DIR . 'layouts' . DS . "shop_sidebar.php" ?>
            </div>
        </div>
    </div>
</section>