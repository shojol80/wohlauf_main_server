<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" <?php print lang_attributes(); ?>>
<head>

    <title>{content_meta_title}</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="{content_meta_title}"/>
    <meta name="keywords" content="{content_meta_keywords}"/>
    <meta name="description" content="{content_meta_description}"/>
    <meta property="og:type" content="{og_type}"/>
    <meta property="og:url" content="{content_url}"/>
    <meta property="og:image" content="{content_image}"/>
    <meta property="og:description" content="{og_description}"/>
    <meta property="og:site_name" content="{og_site_name}"/>






    <script>
        mw.require('icon_selector.js');
        mw.lib.require('bootstrap4');
        mw.lib.require('bootstrap_select');

        mw.iconLoader()
            .addIconSet('fontAwesome')
            .addIconSet('iconsMindLine')
            .addIconSet('iconsMindSolid')
            .addIconSet('mwIcons')
            .addIconSet('materialIcons');
    </script>

    <script>
        $(document).ready(function () {
            $('.selectpicker').selectpicker();
        });
    </script>

    <!-- Plugins Styles -->
    <link href="<?php print template_url(); ?>assets/plugins/magnific-popup/magnific-popup.css" rel="stylesheet"/>

    <link href="<?php print template_url(); ?>assets/js/libs/swiper/css/swiper.min.css" rel="stylesheet"/>

    <link href="<?php print template_url(); ?>assets/css/typography.css" rel="stylesheet"/>

    <?php print get_template_stylesheet(); ?>
    <link rel="stylesheet" href="<?php print template_url(); ?>assets/css/material_icons/material_icons.css" type="text/css"/>
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet"/>
    <link href="<?php print template_url(); ?>assets/css/custom.css" rel="stylesheet"/>
    <link href="<?php print template_url(); ?>assets/css/style.css" rel="stylesheet"/>
    <link href="<?php print template_url(); ?>assets/css/newstyle.css" rel="stylesheet"/>
    <link href="<?php print template_url(); ?>assets/css/responsive.css" rel="stylesheet"/>

    <link href="<?php print template_url(); ?>assets/css/responsive.css" rel="stylesheet"/>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script type="text/javascript" src="<?php print template_url(); ?>assets/js/jquery-ui.js"></script>
    <?php include('template_settings.php'); ?>
</head>
<body class="<?php print helper_body_classes(); ?> <?php print 'member-nav-inverse ' . $header_style . ' ' . $sticky_navigation; ?> ">
<!-- <div class="custom-modal">
    <div class="exit-intent-popup" role="alert">

        <div class="newsletter">
        <span class="close" id='close_popup'>&times;</span>
            <div class="text-center">
                <div class="edit" rel="content" field="test_content">
                    <module type="layouts" template="skin-11"/>
                </div>
            </div>
        </div>
    </div>
</div> -->

<div class="main">
    <div class="navigation-holder">
        <nav class="navigation">
            <div class="container">
                <?php
                    use Illuminate\Support\Facades\DB;
                    $headerShowCss = "none";
                    $temps = DB::table('header_show_hides')->get();
                    if($temps->count() > 0){
                        $pageCheck = false;
                        foreach( $temps as $temp){
                            $tempv = DB::table('content')->where('id',$temp->page_id)->get();
                            if(Request::url() == url('/').'/'.$tempv[0]->url){
                                $pageCheck = true;
                            }
                        }

                        if($pageCheck == true){
                            $headerShowCss = "none";
                        }
                        else{
                            $headerShowCss = "flex";
                        }
                    }
                    else{
                        $headerShowCss = "flex";
                    }

                ?>
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header" style="display:<?php print $headerShowCss; ?>;">
                    <module type="logo" class="logo" id="header-logo"/>

                    <div class="menu">
                        <module type="menu" id="header-menu" template="navbar"/>

                        <ul class="list mobile-list">
                            <?php if ($profile_link == 'true'): ?>
                                <li class="mobile-profile">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><i class="fa fa-user-circle-o"></i> <span><?php print user_name(); ?> <span class="caret"></span></span></a>
                                    <ul class="dropdown-menu">
                                        <?php if (user_id()): ?>
                                            <li><a href="#" data-toggle="modal" data-target="#loginModal"><?php _lang("Profil", 'templates/bamboo'); ?></a></li>
                                            <li><a href="#" data-toggle="modal" data-target="#ordersModal"><?php _lang("
meine Bestellungen", 'templates/bamboo'); ?></a></li>
                                        <?php else: ?>
                                            <li><a href="#" data-toggle="modal" data-target="#loginModal"><?php _lang("Anmeldung", 'templates/bamboo'); ?></a></li>
                                        <?php endif; ?>

                                        <?php if (is_admin()): ?>
                                            <li><a href="<?php print admin_url() ?>"><?php _lang("Administrationsmen�", 'templates/bamboo'); ?></a></li>
                                        <?php endif; ?>

                                        <?php if (user_id()): ?>
                                            <li><a href="<?php print api_link('logout') ?>"><?php _lang("Ausloggen", 'templates/bamboo'); ?></a></li>
                                        <?php endif; ?>
                                    </ul>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>


                <div style="display:flex;align-items:center">


                    <div class="toggle">
                        <a href="javascript:;" class="js-menu-toggle">
                            <span class="mobile-menu-label">
                            </span>
                            <span class="mobile-menu-btn">
                                <span></span>
                                <span></span>
                                <span></span>
                            </span>
                        </a>
                    </div>

                    <ul class="member-nav">

                            <li class="search dropdown ml-4">
                                <button class="btn-search dropdown-toggle" type="button" id="dropdown_search" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fa fa-search"></i>
                                </button>
                                <div class="dropdown-menu" aria-labelledby="dropdown_search">
                                    <form class="d-flex flex-nowrap search-header-form" action="<?php print site_url(); ?>search" method="get">
                                        <input type="search" placeholder="" id="keywords" name="keywords"/>
                                        <button class="btn" type="submit"> Search</button>
                                    </form>
                                </div>
                            </li>


                        <?php if ($shopping_cart == 'true'): ?>
                            <li class="dropdown btn-cart">
                                <a href="#" class="dropdown-toggle" onclick="carttoggole()"><span class="material-icons cart-icon">
shopping_cart
</span> <span id="shopping-cart-quantity" class="js-shopping-cart-quantity"><?php print cart_sum(false); ?></span></a>
<div id="checkout-product" style="display: none;">
                                        <module type="shop/cart" template="quick_checkout" />
                                    </div>
                            </li>
                        <?php endif; ?>


                            <li class="dropdown btn-member">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span><?php if (user_id()): ?><?php print user_name(); ?><?php else: ?><?php echo _e('Einloggen'); ?><?php endif; ?> <span class="caret"></span></span></a>
                                <ul class="dropdown-menu">
                                    <?php if (user_id()): ?>
                                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><?php _lang("Profil", 'templates/bamboo'); ?></a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#ordersModal"><?php _lang("meine Bestellungen", 'templates/bamboo'); ?></a></li>
                                    <?php else: ?>
                                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><?php _lang("Anmeldung", 'templates/bamboo'); ?></a></li>
                                    <?php endif; ?>

                                    <?php if (is_admin()): ?>
                                        <li><a href="<?php print admin_url() ?>"><?php _lang("Administrationsmenü", 'templates/bamboo'); ?></a></li>
                                    <?php endif; ?>

                                    <?php if (user_id()): ?>
                                        <li><a href="<?php print api_link('logout') ?>"><?php _lang("Ausloggen", 'templates/bamboo'); ?></a></li>
                                    <?php endif; ?>
                                </ul>
                            </li>

                    </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <script>
	  $(document).ready(function(){
		  $('.js-example-basic-multiple').select2();
	  });
  </script>
<script type="text/javascript">
  function carttoggole() {
        $("#cartModal").modal('show');
    }

</script>

<!-- Login Modal -->
<div class="modal login-modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">
                <div class="js-login-window">
                    <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                    <module type="users/login" id="loginModalModuleLogin" />
                    <!-- <div type="users/login" id="loginModalModuleLogin"></div> -->
                </div>

                <div class="js-register-window" style="display:none;">
                <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                    <module type="users/register" id="loginModalModuleRegister" />

                    <p class="or"><span>oder</span></p>

                    <div class="act login">
                        <a href="#" class="js-show-login-window"><span><i class="fa fa-backward" style="margin-right: 10px;" aria-hidden="true"></i>Zurück zur Anmeldung</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!--Cart Modal -->
<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <div id="checkout-product">
              <module type="shop/cart" template="quick_checkout" />
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>




<script>
    $(document).ready(function () {
        $('#loginModal').on('show.bs.modal', function (e) {
            $('#loginModalModuleLogin').reload_module();
            $('#loginModalModuleRegister').reload_module();
        })


        // $('#shoppingCartModal').on('show.bs.modal', function (e) {
        //     $('#js-ajax-cart-checkout-process').reload_module();
        // })


        <?php if (isset($_GET['mw_payment_success'])) { ?>
        $('#js-ajax-cart-checkout-process').attr('mw_payment_success', true);
        // $('#shoppingCartModal').modal('show')

        <?php } ?>


        $('.js-show-register-window').on('click', function () {
            $('.js-login-window').hide();
            $('.js-register-window').show();
        })
        $('.js-show-login-window').on('click', function () {

            $('.js-register-window').hide();
            $('.js-login-window').show();
        })
    })
</script>
