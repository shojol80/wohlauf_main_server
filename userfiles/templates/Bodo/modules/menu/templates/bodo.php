<?php

/*

type: layout

name: Bodo Vertical

description: Bodo Menu skin

*/

?>

<script>
    $(document).ready(function(){
        $(".module-menu-bodo a").wrapInner('<span></span>');
    });
</script>
<div class="module-menu-bodo">
<?php
        $mt = menu_tree($menu_filter);
        if($mt != false){
            print ($mt);
        }
        else{
            print lnotif("There are no items in the menu <b>".$params['menu-name']. '</b>');
        }
?>
</div>
