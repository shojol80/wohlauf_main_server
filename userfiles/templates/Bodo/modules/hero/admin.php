<?php only_admin_access(); ?>

<style>

    #module-logo-settings,
    #module-logo-settings *{
      -webkit-box-sizing: border-box;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
    }

     #hello_world_module{
       padding: 20px;
     }
     #hello_world_module label{
       display: block;
       padding: 8px 0 3px;
     }
     #font-and-text{
       width: 100%;
     }
     #font-and-text, #font-and-text *{
       vertical-align: middle;
     }

     .image-row,
     .image-row *{
       vertical-align: middle;
     }

     .the-image-holder{
       width: 200px;
     }

     .the-image{
       margin-right: 12px;
       max-width: 170px;
       max-height: 110px;
       background-color: #eee;
     }

     .the-image[src='']{
       width: 133px;
       height: 100px;

       }

     #sizeslider,
     #fontsizeslider{
       width: 135px;
     }
     #google-fonts{
       width: 100%;
     }
     #module-logo-settings .mw-ui-box-content{
       padding: 20px;
     }
     #theimg img,
     #theimg video{
         width: 200px;
     }

</style>

<?php

    $file =  get_option('file', $params['id']);

    if($file == false){
         if(isset($params['file'])){
            $file = $params['file'];
         }
    }


    $alpha = get_option('alpha', $params['id']);

    if($alpha == false){
        $alpha = 0;
        if(isset($params['overlay-alpha'])){
            $alpha = $params['overlay-alpha'];
        }
    }


    $paralax = get_option('paralax', $params['id']);


    if($paralax === false){
        $paralax = true;
        if(isset($params['paralax'])){
            $paralax = $params['paralax'] == 'true';
        }
    }

    if($paralax === NULL){
        $paralax = false;
    }

    if($alpha === NULL){
        $alpha = 0;
    }




?>
<div class="module-live-edit-settings" id="module-logo-settings">



    <div class="mw-ui-field-holder">
        <div class="mw-ui-box mw-ui-box-content">
            <div class="mw-ui-row-nodrop image-row">

            <div class="mw-ui-col the-image-holder">
                <span id="theimg"><?php if ($file == false) {

        } else { ?>
               <?php if (preg_match('/(\.jpg|\.png|\.bmp|\.gif|\.jpeg)$/', $file)) { ?>
                    <img src="<?php print $file; ?>" alt="" />
               <?php } else{ ?>
                    <video controls src="<?php print $file; ?>" class="mw-paralax-image"></video>
               <?php } ?>
        <?php } ?></span>
          <br><br><br>

                <span class="mw-ui-btn" id="upload-image"><span class="mw-icon-upload"></span>Upload Image/Video</span>

                <div id="progress" style="display: none;margin-top:10px;"></div>

            </div>


            </div>
        </div>
    </div>

    <div class="mw-ui-field-holder">
        <label class="mw-ui-label">Alpha</label>
        0 <input type="range" class="mw_option_field" name="alpha" value="<?php print $alpha; ?>" min="0" max="1" step=".01" /> 1
    </div>
    <div class="mw-ui-field-holder">
        <label class="mw-ui-check">
            <input type="checkbox" class="mw_option_field" value="true" name="paralax" <?php print $paralax ? 'checked' : ''; ?> /><span></span> <span>Paralax</span></label>
    </div>
    <input type="hidden" class="mw_option_field" name="file" id="file" />
    </div>











<script>

$(document).ready(function(){
    UP = mw.uploader({
        element:mwd.getElementById('upload-image'),
        filetypes:'images,videos',
        multiple:false
    });

    $(UP).bind('FileUploaded', function(a,b){
        mw.$("#file").val(b.src).trigger('change');
        if((b.src.match(/\.(jpeg|jpg|gif|png|bpm)$/) != null)){
            mw.$("#theimg").show().html('<img src="'+b.src+'" alt="" />');
        }
        else{
            mw.$("#theimg").show().html('<video src="'+b.src+'" controls></video>');
        }


    });

    UP.progress = mw.progress({
        action:'Loading',
        element: document.getElementById('progress')
   });

    $(UP).bind('progress', function(up, file) {
        $(document.getElementById('progress')).show()
        UP.progress.set(file.percent);
        if(file.percent == 100){
            setTimeout(function(){
                $(document.getElementById('progress')).hide()
            }, 72);
        }
     });




});

</script>