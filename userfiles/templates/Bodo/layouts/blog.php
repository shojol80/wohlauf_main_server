<?php

/*

  type: layout
  content_type: dynamic
  name: Blog
  position: 5
  description: Blog
  tag: blog

*/

?>

<?php include template_dir() . "header.php"; ?>

    <div class="edit" rel="content" field="bodo_content">
        <section class="grey-bg nodrop safe-mode" id="blog">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="title-small-center text-center"><span class="safe-element">Blog</span></h3>

                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <p class="content-details text-center">
                                    Lorem ipsum dolor sit amet consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.
                                </p>
                            </div>
                        </div>

                        <module type="posts" id="posts-<?php print CONTENT_ID; ?>" data-show="thumbnail,title,description,date"/>
                    </div>
                </div>
            </div>
        </section>
    </div>

<?php include template_dir() . "footer.php"; ?>