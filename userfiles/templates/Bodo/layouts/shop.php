<?php

/*

  type: layout
  content_type: dynamic
  name: Shop
  position: 5
  description:Shop layout
  tag: about

*/

?>
<?php include template_dir() . "header.php"; ?>

<div class="edit" rel="content" field="bodo_content">
    <section class="grey-bg nodrop safe-mode" id="employement">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="row">
                        <h3 class="title-small"><span class="safe-element">Shop</span></h3>
                        <p class="content-detail">
                            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                        </p>
                    </div>
                    <div class="row">
                         <?php include TEMPLATE_DIR . 'layouts' . DS . "shop_sidebar.php" ?>
                    </div>
                </div>
                <div class="col-md-9 content-right">
                        <module type="shop/products" id="shop-<?php print CONTENT_ID; ?>">
                </div>
            </div>
        </div>
    </section>
</div>

<?php include template_dir() . "footer.php"; ?>
