<?php

/*

type: layout
content_type: static
name: Text Page
position: 2
is_default: true
description: Text page layout

*/


?>
<?php include template_dir() . "header.php"; ?>

    <div class="container">
        <div class="mw-static-element mw-simple-title-and-text">
            <div class="info">
                <h1 class="edit" rel="content" field="title">Simple title</h1>
                <div class="edit" rel="content" field="bodo_content">
                    <p>
                        Template layout is ready for edit in ream time with Microweber.<br/>
                        There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look
                        even
                        slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum
                        generators on
                        the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined
                    </p>
                </div>
            </div>
        </div>
    </div>

    <div class="edit" rel="content" field="bodo_content">
        <!-- Do not delete this comment! It is for PHP Parser -->
    </div>

<?php include template_dir() . "footer.php"; ?>