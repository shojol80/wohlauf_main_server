<!DOCTYPE HTML>
<html prefix="og: http://ogp.me/ns#">
<head>
    <title>{content_meta_title}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta property="og:title" content="{content_meta_title}"/>
    <meta name="keywords" content="{content_meta_keywords}"/>
    <meta name="description" content="{content_meta_description}"/>
    <meta property="og:type" content="{og_type}"/>
    <meta property="og:url" content="{content_url}"/>
    <meta property="og:image" content="{content_image}"/>
    <meta property="og:description" content="{og_description}"/>
    <meta property="og:site_name" content="{og_site_name}"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <link rel="alternate" type="application/rss+xml" title="{og_site_name}" href="<?php print site_url('rss'); ?>"/>

    <!--styles-->
    <link href="<?php print template_url(); ?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/owl.carousel.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/owl.theme.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/magnific-popup.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/style.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/design.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/responsive.css" rel="stylesheet">
    <link href="<?php print template_url(); ?>css/anotherresponsive.css" rel="stylesheet">

    <!--fonts google-->
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700' rel='stylesheet' type='text/css'>

    <!--[if lt IE 9]>
    <script type="text/javascript" src="<?php print template_url(); ?>js/html5shiv.min.js"></script>
    <![endif]-->

    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />


    <script type="text/javascript" src="<?php print template_url(); ?>js/jquery-ui.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <script>
        mw.require('icon_selector.js');

        mw.iconLoader()
            .addIconSet('iconsMindLine')
            .addIconSet('iconsMindSolid')
            .addIconSet('materialDesignIcons')
            .addIconSet('mwIcons')
            .addIconSet('materialIcons');
    </script>

    <script>
        AddToCartModalContent = window.AddToCartModalContent || function (title) {
                var html = ''

                    + '<section class="text-center">'

                    + '<span class="sm-icon-bag2"></span>'

                    + '<h5>' + title + '</h5>'

                    + '<p class="m-t-10"><?php _e("has been added to your cart"); ?></p>'

                    + '<a href="javascript:;" onclick="mw.tools.modal.remove(\'#AddToCartModal\')" class="mw-ui-btn m-10"><?php _e("Continue shopping"); ?></a> '

                    + '<a href="<?php print checkout_url(); ?>" class="mw-ui-btn mw-ui-btn-invert m-10"><?php _e("Checkout"); ?></a></section>';

                return html;
            }
    </script>
</head>
<body>




<div class="header">
    <div class="row">
        <div class="col-xs-2">
            <module type="logo" image="<?php print template_url(); ?>images/logo.png" size="45">
        </div>
        <div class="col-xs-10">
            <div class="header-right">
                <div class="header-cart-menu">
                    <div class="header-option-btns">

                        <!-- Header Cart -->
                        <div class="header-account">

                                <a href="#" class="dropdown-toggle" onclick="carttoggole()">
                                <span class="material-icons cart-icon">
                                    shopping_cart
                                    </span> (<span id="shopping-cart-quantity" class="js-shopping-cart-quantity"><?php print cart_sum(false); ?>)</span></a>
                                    <div id="checkout-product" style="display: none;">
                                    <module type="shop/cart" template="quick_checkout" />
                                </div>

                        </div>

                        <!-- header Account -->
                        <div class="header-account">
                            <div class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span><?php if (user_id()): ?><?php print user_name(); ?><?php else: ?><?php echo _e('Einloggen'); ?><?php endif; ?> <span class="caret"></span></span></a>
                                <ul class="dropdown-menu">
                                    <?php if (user_id()): ?>
                                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><?php _lang("Profil", 'templates/bamboo'); ?></a></li>
                                        <li><a href="#" data-toggle="modal" data-target="#ordersModal"><?php _lang("meine Bestellungen", 'templates/bamboo'); ?></a></li>
                                    <?php else: ?>
                                        <li><a href="#" data-toggle="modal" data-target="#loginModal"><?php _lang("Anmeldung", 'templates/bamboo'); ?></a></li>
                                    <?php endif; ?>

                                    <?php if (is_admin()): ?>
                                        <li><a href="<?php print admin_url() ?>"><?php _lang("Administrationsmenü", 'templates/bamboo'); ?></a></li>
                                    <?php endif; ?>

                                    <?php if (user_id()): ?>
                                        <li><a href="<?php print api_link('logout') ?>"><?php _lang("Ausloggen", 'templates/bamboo'); ?></a></li>
                                    <?php endif; ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <button class="menu-button" id="open-button">
                    <span></span>
                    <span></span>
                    <span></span>
                </button><!--/.for-sticky-->
            </div>
        </div>
        <div class="menu-wrap">
            <nav class="menu">
                <div class="menu-list">
                    <module type="menu" template="bodo" id="main-menu">
                </div>
            </nav>
            <button class="close-button" id="close-button">Close Menu</button>
        </div>
    </div>
</div>
</div>





<!--/.HEADER END-->

<!--CONTENT WRAP-->
<div class="content-wrap">
    <!--CONTENT-->
<div class="content">




<script>
    $(document).ready(function(){
        $('.js-example-basic-multiple').select2();
    });
</script>
    <script type="text/javascript">
    function carttoggole() {
        var x = document.getElementById("checkout-product");
        if (x.style.display === "block") {
            // console.log('block');
            x.style.display = "none";
        } else {
            // console.log('none');

            x.style.display = "block";
        }
    }

</script>

<!-- Login Modal -->
<div class="modal login-modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>

            <div class="modal-body">
                <div class="js-login-window">
                    <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                    <module type="users/login" id="loginModalModuleLogin" />
                    <!-- <div type="users/login" id="loginModalModuleLogin"></div> -->
                </div>

                <div class="js-register-window" style="display:none;">
                <div class="icon"><i class="fa fa-user" aria-hidden="true"></i></div>
                    <module type="users/register" id="loginModalModuleRegister" />


                    <p class="or"><span>oder</span></p>

                    <div class="act login">
                        <a href="#" class="js-show-login-window"><span></i>Zurück zur Anmeldung</span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php if(!isset($_GET['slug'])){ ?>
<div class="modal" id="shoppingCartModal" tabindex="-1" role="dialog" aria-labelledby="shoppingCartModalLabel">

        <module type="shop/cart" template="small-customized"/>

</div>
<?php } ?>

<script>
    $(document).ready(function () {
        $('#loginModal').on('show.bs.modal', function (e) {
            $('#loginModalModuleLogin').reload_module();
            $('#loginModalModuleRegister').reload_module();
        })


        $('#shoppingCartModal').on('show.bs.modal', function (e) {
            $('#js-ajax-cart-checkout-process').reload_module();
        })


        mw.on('mw.cart.add', function (event, data) {
            $('#shoppingCartModal').modal('show');

        })



        <?php if (isset($_GET['mw_payment_success'])) { ?>
        $('#js-ajax-cart-checkout-process').attr('mw_payment_success', true);
        $('#shoppingCartModal').modal('show')

        <?php } ?>


        $('.js-show-register-window').on('click', function () {
            $('.js-login-window').hide();
            $('.js-register-window').show();
        })
        $('.js-show-login-window').on('click', function () {

            $('.js-register-window').hide();
            $('.js-login-window').show();
        })
    })
</script>
