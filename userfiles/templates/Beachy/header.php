<!DOCTYPE html>
<html prefix="og: http://ogp.me/ns#" <?php print lang_attributes(); ?>>

<head>

    <title>{content_meta_title}</title>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>

    <meta property="og:title" content="{content_meta_title}"/>
    <meta name="keywords" content="{content_meta_keywords}"/>
    <meta name="description" content="{content_meta_description}"/>
    <meta property="og:type" content="{og_type}"/>
    <meta property="og:url" content="{content_url}"/>
    <meta property="og:image" content="{content_image}"/>
    <meta property="og:description" content="{og_description}"/>
    <meta property="og:site_name" content="{og_site_name}"/>
    <script>
        mw.require('icon_selector.js');
        mw.lib.require('bootstrap4');
        mw.lib.require('bootstrap_select');

        mw.iconLoader()
            .addIconSet('fontAwesome')
            .addIconSet('iconsMindLine')
            .addIconSet('iconsMindSolid')
            .addIconSet('materialDesignIcons')
            .addIconSet('mwIcons')
            .addIconSet('materialIcons');
    </script>

    <script>
        $(document).ready(function () {
            $('.selectpicker').selectpicker();
        });
    </script>
    <?php
        //Seo data for google anaylytical
        print (template_head(true));
        $is_installed_status = Config::get('microweber.is_installed');
        if(!empty($is_installed_status)){
            if(function_exists('basicGoogleAnalytical')){
                basicGoogleAnalytical();
            }
        }
        //end code
    ?>

<link rel="stylesheet" href="<?php print template_url(); ?>assets/css/main-style.css" type="text/css"/>
<link rel="stylesheet" href="<?php print template_url(); ?>assets/css/responsive.css" type="text/css"/>
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <?php print get_template_stylesheet(); ?>
    <link rel="stylesheet" href="<?php print template_url(); ?>assets/css/typography.css" type="text/css"/>

    <link href="<?php print template_url(); ?>dist/main.min.css" rel="stylesheet"/>

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="<?php print template_url(); ?>assets/js/jquery-ui.js"></script>
    <?php include('template_settings.php'); ?>
</head>
<body class="<?php print helper_body_classes(); ?> <?php print $sticky_navigation; ?> ">
<input type="hidden" id="page_id_for_layout_copy" value="<?=PAGE_ID;?>">

<?php
    use Illuminate\Support\Facades\DB;
    $headerShowCss = "none";
    $temps = DB::table('header_show_hides')->get();
    if($temps->count() > 0){
        $pageCheck = false;
        foreach( $temps as $temp){
            $tempv = DB::table('content')->where('id',$temp->page_id)->get();
            if(Request::url() == url('/').'/'.$tempv[0]->url){
                $pageCheck = true;
            }
        }

        if($pageCheck == true){
            $headerShowCss = "none";
        }
        else{
            $headerShowCss = "flex";
        }
    }
    else{
        $headerShowCss = "flex";
    }

    $sidebar = Config::get('custom.sidebar');
    $header = Config::get('custom.header');
    if(@$header == 'header'){
        $showHeader = showCat('header');
    }else if(@$sidebar == 'sidebar'){
        $showHeader = showCat('sidebar');
    }else{
        Config::set('custom.sidebar', 'sidebar');
        Config::save(array('custom'));
    }


?>

<div class="main">
    <div class="navigation-holder <?php print $header_style; ?><?php if ($search_bar == 'false'): ?> no_search_bar <?php endif; ?>">
        <?php if ($header_style == 'header_style_1'): ?>
            <?php include('partials/header/header_style_1.php'); ?>
        <?php elseif ($header_style == 'header_style_2'): ?>
            <?php include('partials/header/header_style_2.php'); ?>
        <?php elseif ($header_style == 'header_style_3'): ?>
            <?php include('partials/header/header_style_3.php'); ?>
        <?php elseif ($header_style == 'header_style_4'): ?>
            <?php include('partials/header/header_style_4.php'); ?>
        <?php elseif ($header_style == 'header_style_5'): ?>
            <?php include('partials/header/header_style_5.php'); ?>
        <?php elseif ($header_style == 'header_style_6'): ?>
            <?php include('partials/header/header_style_6.php'); ?>
        <?php elseif ($header_style == 'header_style_7'): ?>
            <?php include('partials/header/header_style_7.php'); ?>
        <?php elseif ($header_style == 'header_style_8'): ?>
            <?php include('partials/header/header_style_8.php'); ?>
        <?php elseif ($header_style == 'header_style_9'): ?>
            <?php include('partials/header/header_style_9.php'); ?>
        <?php else: ?>
            <?php include('partials/header/header_style_1.php'); ?>
        <?php endif; ?>
    </div>


<?php
    dt_url_redirect_redirectUrl();
?>