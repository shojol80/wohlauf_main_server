<?php if ($footer == 'true'): ?>
    <footer class="p-t-60 p-b-60 edit" rel="content" field="footer-main-wrapper">
        <div class="container">
            <div class="edit nodrop safe-mode" field="new-world_footer" rel="global">
                <div class="row">
                    <div class="col-12 col-lg-12 col-xl-2 mx-auto logo-column text-center m-b-20 allow-drop">
                        <img src="<?php print template_url(); ?>assets/img/logo_footer.png" alt="" class="m-b-10"/>
                        <br/>
                        <br/>
                    </div>

                    <div class="col-12 col-lg-12 col-xl-6 mx-auto text-center m-b-40 allow-drop">
                        <module type="menu" template="simple" id="footer_menu" name="footer_menu"/>
                        <br/>
                        <p><?php print _lang('Droptienda is free open source drag and dop website builder and CMS. It is under MIT license and we use Laravel  PHP framework', 'templates/new-world'); ?></p>
                    </div>

                    <div class="col-12 col-sm col-lg-7 col-xl mx-auto text-white text-center allow-drop">
                        <h6 class="m-t-5">Social Networks</h6>

                        <module type="social_links" id="footer_socials"/>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-md-12 text-center">
                    <p>Shopsystem & Template by <a href="https://www.droptienda.com/" target="_blank">Droptienda®</a> </p>
                </div>
            </div>
        </div>
    </footer>

    <div class="bg-pines d-block d-lg-none bg-default" style="height: 50px;"></div>
<?php endif; ?>

</div>

<button id="to-top" class="btn" style="display: block;"></button>

<?php include('footer_cart.php'); ?>


<script>
    mw.lib.require('slick');
    mw.lib.require('collapse_nav');
</script>

<script src="<?php print template_url(); ?>dist/main.min.js"></script>

<!--Term  Modal -->
<div class="modal fade" id="termModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      </div>
      <div class="modal-body">
        <div class="termModal-content">
            <module type="legals/shipping-info"/>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>
<script>


    jQuery(window).on('load', function(){
        if(jQuery(".categorySideBar .module-categories>.well>ul.nav>li").children("ul").length) {
            jQuery(".categorySideBar .module-categories>.well>ul.nav>li").children("ul").parent().addClass("hasSubMenu");
            jQuery(".categorySideBar .module-categories>.well>ul.nav>li").children("ul").parent().append("<span class='hs-toggle'></span>");
        }

        jQuery(".hs-toggle").on("click", function(){
            // jQuery(".module-categories").toggleClass("showSub");
            //$(".hs-toggle").parent().removeClass("showThisSub");
            $(this).parent().toggleClass("showThisSub");

        });


        if(jQuery(".categorySideBar .module-categories>.well>ul.nav>li").length>5){
            jQuery(".categorySideBar").append("<span class='viewMoreCategory'>weitere anzeigen</span>");
        }

        jQuery(".viewMoreCategory").on("click", function(){
            jQuery(".categorySideBar .module-categories>.well>ul.nav").toggleClass("show_ucmAll");

            var currentVMbtnText = jQuery(".viewMoreCategory").text();
            if (currentVMbtnText === "weitere anzeigen") {
                jQuery(".viewMoreCategory").html("ausblenden");
            } else {
                jQuery(".viewMoreCategory").html("weitere anzeigen");
            }
            //alert(currentVMbtnText);
        });

    });


</script>






</body>
</html>
