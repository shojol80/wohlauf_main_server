<?php if ($shopping_cart == 'true'): ?>
    <li class="dropdown btn-cart ml-4">
        <a href="#" class="dropdown-toggle" onclick="carttoggole()"><i class="fa fa-shopping-cart"></i> <span class="js-shopping-cart-quantity"><?php print cart_sum(false); ?></span></a>
    </li>
<?php endif; ?>

<script type="text/javascript">
  function carttoggole() {
        $("#cartModal").modal('show');
    }

</script>

<!--Cart Modal -->
<div class="modal fade" id="cartModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-body">
          <div id="checkout-product">
              <module type="shop/cart" template="quick_checkout" />
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Schließen</button>
      </div>
    </div>
  </div>
</div>